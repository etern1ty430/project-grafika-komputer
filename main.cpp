#include <windows.h>
#include <math.h>
#include <iostream>
#include <GL/gl.h>
#include "glut.h"
#include <Windows.h>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <stdlib.h>


using namespace std;

struct Objek{
	vector< vector<float> > vektor;
	vector< vector<float> > vektorNormal;
	vector< vector<int> > faces;
};

int refreshmil=1;
GLfloat angle=45.0f;
Objek Benda[8];
//Sudut rotasi kamera
float angleX = 0.0f, angleY = 0.0f;
float cameraVectorX = 0.0f, cameraVectorZ = -1.0f, cameraVectorY = 0.0f;
float cameraX = -3.0f, cameraZ = 1.0f, cameraY = 3.0f;
float deltaAngleX = 0.0f, yOrigin = -1, xOrigin = -1, deltaAngleY = 0.0f;


int w, h;
GLuint textkopi,textmejakopi,textmejatv,textsofa,texttv,textaluminium,texttembok,textsmudge,textcanvassofa,text_semen,text_pink,text_lemari_kayu;
GLuint texture[13];
unsigned char texturedata[13][512*512*3];

GLfloat posisiBenda[8][3] = { { 1,1,0 }, { 1,0,0 }, { 1,0,-4 }, { 1,0,6}, { 1,1.2,-4 },{25,7.5,0},{20,0,8},{29.5,-0.5,8}};

vector<string> split (const string &s, char delim) {
    vector<string> result;
    stringstream ss (s);
    string item;
    while (getline (ss, item, delim)) {
        result.push_back (item);
    }
    return result;
}
vector<string> splitstring (string s, string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    string token;
    vector<string> res;
    while ((pos_end = s.find (delimiter, pos_start)) != string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }
    res.push_back (s.substr (pos_start));
    return res;
}
//ini lighting nya:)
void pasanglampu(){
    glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER,GL_TRUE);
    glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	GLfloat posisilampu[]={20,5,-20,1};
	GLfloat color[]={1,1,1,1};

	glLightfv(GL_LIGHT0,GL_POSITION,posisilampu);
	glLightfv(GL_LIGHT0,GL_DIFFUSE,color);

}
void LoadBMP(char *fname, unsigned char *buf, int& w, int& h)
{
	HANDLE handle;
	BITMAPFILEHEADER fileheader;
	BITMAPINFOHEADER infoheader;
	unsigned long read;
	unsigned long palettelength;
	RGBQUAD palette[256];
	int width, height;
	unsigned long bitmaplength;
	handle=CreateFile(fname, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
	if (handle==INVALID_HANDLE_VALUE) exit(1);
	ReadFile(handle, &fileheader, sizeof(fileheader), &read, 0);
	ReadFile(handle, &infoheader, sizeof(infoheader), &read, 0);
	palettelength = infoheader.biClrUsed;
	ReadFile(handle, palette, palettelength, &read, 0);
	if (read!=palettelength) exit(1);
	width = infoheader.biWidth;
	height = infoheader.biHeight;
	bitmaplength = infoheader.biSizeImage;
	if (bitmaplength==0)
		bitmaplength = width*height*infoheader.biBitCount / 8;
	ReadFile(handle, buf, bitmaplength, &read, 0);
	if (read!=bitmaplength) exit(1);

	CloseHandle(handle);
	for(unsigned long i=0;i<bitmaplength;i+=3)
		{
		unsigned char tmp;
		tmp=buf[i];
		buf[i]=buf[i+2];
		buf[i+2]=tmp;
		}
	w=width; h=height;
}

void LoadTexture(char *fn, unsigned char *buf, GLuint& id)
{
	GLuint tex1;
	glEnable(GL_TEXTURE_2D);
	LoadBMP(fn, buf, w, h);
	glGenTextures(1, &tex1);
	id = tex1;
	glBindTexture(GL_TEXTURE_2D, id);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, buf);
}

void setupTexture(){
    LoadTexture("textkopi.bmp",texturedata[2],textkopi);
    LoadTexture("textmejakopi.bmp",texturedata[3],textmejakopi);
    LoadTexture("textmejatv.bmp",texturedata[4],textmejatv);
    LoadTexture("textsofa.bmp",texturedata[5],textsofa);
    LoadTexture("texttv.bmp",texturedata[6],texttv);
    LoadTexture("texttembok.bmp",texturedata[7],texttembok);
    LoadTexture("smudge.bmp",texturedata[8],textsmudge);
    LoadTexture("canvas_sofa.bmp",texturedata[9],textcanvassofa);
    LoadTexture("text_tembok_semen.bmp",texturedata[10],text_semen);
    LoadTexture("lantai_pink.bmp",texturedata[11],text_pink);
    LoadTexture("white_lemari.bmp",texturedata[12],text_lemari_kayu);

}

void loadObj(string filename, int index){
    ifstream inFile;
    string line;
    inFile.open(filename.c_str());
    if (!inFile) {
        cout << "Unable to open file";
    }else{
        while (getline(inFile, line)) {
            char kataPertama = line[0];
            if(kataPertama == 'f'){
                line = line.substr(2);
                vector<string> splitted = split(line, ' ');
                vector<string> pisahPertama = splitstring(splitted[0], "//");
                vector<string> pisahKedua = splitstring(splitted[1], "//");
                vector<string> pisahKetiga = splitstring(splitted[2], "//");
                vector<int> face;
                int temp1, temp2;
                stringstream(pisahPertama[0]) >> temp1;
                stringstream(pisahPertama[1]) >> temp2;
                face.push_back(temp1);
                face.push_back(temp2);

                stringstream(pisahKedua[0]) >> temp1;
                stringstream(pisahKedua[1]) >> temp2;
                face.push_back(temp1);
                face.push_back(temp2);

                stringstream(pisahKetiga[0]) >> temp1;
                stringstream(pisahKetiga[1]) >> temp2;
                face.push_back(temp1);
                face.push_back(temp2);
                Benda[index].faces.push_back(face);
                face.clear();
            }else{
                char kataKedua = line[1];
                if(kataKedua == 'n'){
                    line = line.substr(3);
                    vector<float> normal;
                    vector<string> splitted = split(line, ' ');
                    float temp1, temp2, temp3;
                    stringstream(splitted[0]) >> temp1;
                    stringstream(splitted[1]) >> temp2;
                    stringstream(splitted[2]) >> temp3;
                    normal.push_back(temp1);
                    normal.push_back(temp2);
                    normal.push_back(temp3);
                    Benda[index].vektorNormal.push_back(normal);
                    normal.clear();
                }else if(line.substr(0,2) == "v "){
                    line = line.substr(2);
                    vector<float> vertex;
                    vector<string> splitted = split(line, ' ');
                    float temp1, temp2, temp3;
                    stringstream(splitted[0]) >> temp1;
                    stringstream(splitted[1]) >> temp2;
                    stringstream(splitted[2]) >> temp3;
                    vertex.push_back(temp1);
                    vertex.push_back(temp2);
                    vertex.push_back(temp3);
                    Benda[index].vektor.push_back(vertex);
                    vertex.clear();
                }
            }
        }
        inFile.close();
    }
}

void keyDown(unsigned char key, int x, int y){
    float fraction = 0.5f;
    if(key == 27) exit(0);
    else if(key == 'w' || key == 'W'){
        cameraX += cameraVectorX * fraction;
        cameraZ += cameraVectorZ * fraction;
        cameraY += cameraVectorY * fraction;
    }else if(key == 'a' || key == 'A'){
        angleX -= 0.1f;
        cameraVectorX = sin(angleX);
        cameraVectorZ = -cos(angleX);
    }else if(key == 's' || key == 'S'){
        cameraX -= cameraVectorX * fraction;
        cameraZ -= cameraVectorZ * fraction;
        cameraY -= cameraVectorY * fraction;
    }else if(key == 'd' || key == 'D'){
        angleX += 0.1f;
        cameraVectorX = sin(angleX);
        cameraVectorZ = -cos(angleX);
    }
}

void mouseButton(int button, int state, int x, int y){
    if(button == GLUT_LEFT_BUTTON){
        if(state == GLUT_UP){
            angleY += deltaAngleY;
            angleX += deltaAngleX;
            xOrigin = -1;
            yOrigin = -1;
        }else{
            xOrigin = x;
            yOrigin = y;
        }
    }
}

void mouseMove(int x, int y){
    if(yOrigin >= 0){
        deltaAngleY = (y - yOrigin) * 0.001f;
        cameraVectorY = sin(angleY + deltaAngleY);
    }
}

void buatRuang(){
    //Draw ground start section
    //lantai besar
    glBindTexture(GL_TEXTURE_2D,text_pink);
    //glNormal3f(0,-1,0);
    glBegin(GL_QUADS);
        glTexCoord2f(-10.0f, -10.0f);
		glVertex3f(-10.0f, -0.5f, -10.0f);
        glTexCoord2f(-10.0f, 12.0f);
		glVertex3f(-10.0f, -0.5f,  12.0f);
        glTexCoord2f(10.0f, 12.0f);
		glVertex3f( 10.0f, -0.5f,  12.0f);
        glTexCoord2f(10.0f, -10.0f);
		glVertex3f( 10.0f, -0.5f, -10.0f);
	glEnd();
	//lantai perbatasan
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,text_pink);
    //glNormal3f(0,-1,0);
    glBegin(GL_QUADS);
        glTexCoord2f(10.0f, -10.0f);
		glVertex3f( 10.0f, -0.5f, -10.0f);
        glTexCoord2f(10.0f, -4.0f);
		glVertex3f( 10.0f, -0.5f, -4.0f);
        glTexCoord2f(15.0f, -4.0f);
		glVertex3f( 15.0f, -0.5f, -4.0f);
        glTexCoord2f(15.0f, -10.0f);
		glVertex3f( 15.0f, -0.5f, -10.0f);
	glEnd();
	//lantai besarr
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,text_pink);
    //glNormal3f(0,-1,0);
    glBegin(GL_QUADS);
        glTexCoord2f(15.0f, -10.0f);
		glVertex3f( 15.0f, -0.5f, -10.0f);
        glTexCoord2f(15.0f, 10.0f);
		glVertex3f( 15.0f, -0.5f,  10.0f);
        glTexCoord2f(30.0f, 10.0f);
		glVertex3f( 30.0f, -0.5f,  10.0f);
        glTexCoord2f(30.0f, -10.0f);
		glVertex3f( 30.0f, -0.5f, -10.0f);
	glEnd();
	//Draw ground end section
    //Draw roof start section
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,textaluminium);
    //glNormal3f(0,1,0);
    glBegin(GL_QUADS);
        glTexCoord2f(-10.0f, -10.0f);
		glVertex3f(-10.0f, 9.5f, -10.0f);
        glTexCoord2f(-10.0f, 12.0f);
		glVertex3f(-10.0f, 9.5f,  12.0f);
        glTexCoord2f(10.0f, 12.0f);
		glVertex3f( 10.0f, 9.5f,  12.0f);
        glTexCoord2f(10.0f, -10.0f);
		glVertex3f( 10.0f, 9.5f, -10.0f);
	glEnd();
	//Draw roof lorong section
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,textaluminium);
    //glNormal3f(0,1,0);
    glBegin(GL_QUADS);
        glTexCoord2f(10.0f, -10.0f);
		glVertex3f( 10.0f, 9.5f, -10.0f);
        glTexCoord2f(10.0f, -4.0f);
		glVertex3f( 10.0f, 9.5f, -4.0f);
        glTexCoord2f(15.0f, -4.0f);
		glVertex3f( 15.0f, 9.5f, -4.0f);
        glTexCoord2f(15.0f, -10.0f);
		glVertex3f( 15.0f, 9.5f, -10.0f);
	glEnd();
    //Draw roof end section
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,textaluminium);
    //glNormal3f(0,1,0);
    glBegin(GL_QUADS);
        glTexCoord2f(15.0f, -10.0f);
		glVertex3f( 15.0f, 9.5f, -10.0f);
        glTexCoord2f(15.0f, 10.0f);
		glVertex3f( 15.0f, 9.5f,  10.0f);
        glTexCoord2f(30.0f, 10.0f);
		glVertex3f( 30.0f, 9.5f,  10.0f);
        glTexCoord2f(30.0f, -10.0f);
		glVertex3f( 30.0f, 9.5f, -10.0f);
	glEnd();

//draw tembok ruangan1
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,texttembok);
    glBegin(GL_QUADS);
        glTexCoord2f(-10.0f, -10.0f);
		glVertex3f(-10.0f,9.5f,-10.0f);
        glTexCoord2f(-10.0f, -10.0f);
		glVertex3f(-10.0f,-0.5f,-10.0f);
        glTexCoord2f(-10.0f, 12.0f);
		glVertex3f(-10.0f,-0.5f,12.0f);
        glTexCoord2f(-10.0f, 12.0f);
		glVertex3f(-10.0f,9.5f,12.0f);
	glEnd();
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,texttembok);
    glBegin(GL_QUADS);
        glTexCoord2f(-10.0f, 12.0f);
		glVertex3f(-10.0f,9.5f,12.0f);
        glTexCoord2f(-10.0f, 12.0f);
		glVertex3f(-10.0f,-0.5f,12.0f);
        glTexCoord2f(10.0f, 12.0f);
		glVertex3f(10.0f,-0.5f,12.0f);
        glTexCoord2f(10.0f, 12.0f);
		glVertex3f(10.0f,9.5f,12.0f);
	glEnd();
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,texttembok);
    glBegin(GL_QUADS);
        glTexCoord2f(-10.0f, -10.0f);
		glVertex3f(-10.0f,-0.5f,-10.0f);
        glTexCoord2f(-10.0f, -10.0f);
		glVertex3f(-10.0f,9.5f,-10.0f);
        glTexCoord2f(30.0f, -10.0f);
		glVertex3f(30.0f,9.5f,-10.0f);
        glTexCoord2f(30.0f, -10.0f);
		glVertex3f(30.0f,-0.5f,-10.0f);
	glEnd();
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,texttembok);
    glBegin(GL_QUADS);
        glTexCoord2f(10.0f, 12.0f);
		glVertex3f( 10.0f,-0.5f,12.0f);
        glTexCoord2f(10.0f, 12.0f);
		glVertex3f( 10.0f,9.5f,12.0f);
        glTexCoord2f(10.0f, -4.0f);
		glVertex3f( 10.0f,9.5f,-4.0f);
        glTexCoord2f(10.0f, -4.0f);
		glVertex3f( 10.0f,-0.5f,-4.0f);
	glEnd();
	//tembok 4-batas-lorong
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,texttembok);
    glBegin(GL_QUADS);
        glTexCoord2f(10.0f, -4.0f);
		glVertex3f(10.0f,-0.5f,-4.0f);
        glTexCoord2f(10.0f, -4.0f);
		glVertex3f(10.0f,9.5f,-4.0f);
        glTexCoord2f(15.0f, -4.0f);
		glVertex3f(15.0f,9.5f,-4.0f);
        glTexCoord2f(15.0f, -4.0f);
		glVertex3f(15.0f,-0.5f,-4.0f);
	glEnd();
	//tembok 5
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,texttembok);
    glBegin(GL_QUADS);
        glTexCoord2f(15.0f, -4.0f);
		glVertex3f(15.0f,-0.5f,-4.0f);
        glTexCoord2f(15.0f, -4.0f);
		glVertex3f(15.0f,9.5f,-4.0f);
        glTexCoord2f(15.0f, 10.0f);
		glVertex3f(15.0f,9.5f,10.0f);
        glTexCoord2f(15.0f, 10.0f);
		glVertex3f(15.0f,-0.5f,10.0f);
	glEnd();
	//tembok 6
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,texttembok);
    glBegin(GL_QUADS);
        glTexCoord2f(15.0f, 10.0f);
		glVertex3f(15.0f,-0.5f,10.0f);
        glTexCoord2f(15.0f, 10.0f);
		glVertex3f(15.0f,9.5f,10.0f);
        glTexCoord2f(30.0f, 10.0f);
		glVertex3f(30.0f,9.5f,10.0f);
        glTexCoord2f(30.0f, 10.0f);
		glVertex3f(30.0f,-0.5f,10.0f);
	glEnd();
	//tembok 7
    glShadeModel(GL_SMOOTH);
    glBindTexture(GL_TEXTURE_2D,texttembok);
    glBegin(GL_QUADS);
        glTexCoord2f(30.0f, 10.0f);
		glVertex3f(30.0f,-0.5f,10.0f);
        glTexCoord2f(30.0f, 10.0f);
		glVertex3f(30.0f,9.5f,10.0f);
        glTexCoord2f(30.0f, -10.0f);
		glVertex3f(30.0f,9.5f,-10.0f);
        glTexCoord2f(30.0f, -10.0f);
		glVertex3f(30.0f,-0.5f,-10.0f);
	glEnd();
//end1
}
void drawObjek(){
    buatRuang();
    float h1=0, h2=0, h3=1, w1=0, w2=1, w3=1;
    for(int k = 0;k < 8;k++){
        glPushMatrix();
        glTranslatef(posisiBenda[k][0], posisiBenda[k][1], posisiBenda[k][2]);
        if(k==0){//kopi
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textkopi);
            glScalef(0.03,0.03,0.03);
            //glTranslatef(posisiBenda[k][0],posisiBenda[k][1]+2,posisiBenda[k][2]);
        }else if(k==1){//meja kopi
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textmejakopi);
            glScalef(1,0.5,0.5);
        }else if(k==2){//meja tv
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textmejatv);
            glScalef(1,1,2);
        }else if(k==3){//sofa
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textsofa);
            glScalef(2,2,2);
            glRotatef(180,0,1,0);
        }else if(k==4){//tv
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, texttv);
            glScalef(0.5,0.5,0.5);
        }else if(k==5){//kipas 2
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, textsmudge);
            glScalef(2,2,2);
            glRotatef(180,1,0,0);
            //untuk putar benda
            glRotatef(angle, 0,1,0);
        }else if(k==6){//sofa 2
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D,textcanvassofa);
            glScalef(3,3,3);
            glRotatef(180,0,1,0);
        }else if(k==7){//lemari 2
            glShadeModel(GL_SMOOTH);
            glBindTexture(GL_TEXTURE_2D, text_lemari_kayu);
            glTranslatef(-0.5,0,-6);
            glScalef(2,2,6);
        }
        for(int i = 0;i < Benda[k].faces.size();i++){
            glBegin(GL_TRIANGLES);
            glNormal3f(
                Benda[k].vektorNormal[Benda[k].faces[i].at(1) - 1].at(0),
                Benda[k].vektorNormal[Benda[k].faces[i].at(1) - 1].at(1),
                Benda[k].vektorNormal[Benda[k].faces[i].at(1) - 1].at(2)
            );
            glTexCoord2f(w1, h1);
            glVertex3f(
                Benda[k].vektor[Benda[k].faces[i].at(0) - 1].at(0),
                Benda[k].vektor[Benda[k].faces[i].at(0) - 1].at(1),
                Benda[k].vektor[Benda[k].faces[i].at(0) - 1].at(2)
            );

            glNormal3f(
                Benda[k].vektorNormal[Benda[k].faces[i].at(3) - 1].at(0),
                Benda[k].vektorNormal[Benda[k].faces[i].at(3) - 1].at(1),
                Benda[k].vektorNormal[Benda[k].faces[i].at(3) - 1].at(2)
            );
            glTexCoord2f(w2, h2);
            glVertex3f(
                Benda[k].vektor[Benda[k].faces[i].at(2) - 1].at(0),
                Benda[k].vektor[Benda[k].faces[i].at(2) - 1].at(1),
                Benda[k].vektor[Benda[k].faces[i].at(2) - 1].at(2)
            );

            glNormal3f(
                Benda[k].vektorNormal[Benda[k].faces[i].at(5) - 1].at(0),
                Benda[k].vektorNormal[Benda[k].faces[i].at(5) - 1].at(1),
                Benda[k].vektorNormal[Benda[k].faces[i].at(5) - 1].at(2)
            );
            glTexCoord2f(w3, h3);
            glVertex3f(
                Benda[k].vektor[Benda[k].faces[i].at(4) - 1].at(0),
                Benda[k].vektor[Benda[k].faces[i].at(4) - 1].at(1),
                Benda[k].vektor[Benda[k].faces[i].at(4) - 1].at(2)
            );
            glEnd();
        }
        glPopMatrix();
    }
}
void render(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glEnable(GL_NORMALIZE);
    glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);


    gluLookAt(
        cameraX, cameraY, cameraZ,
        cameraX + cameraVectorX, cameraY + cameraVectorY, cameraZ + cameraVectorZ,
        0.0f, 1.0f, 0.0f
    );

    pasanglampu();

    drawObjek();
	glutSwapBuffers();
    angle+=20;
}

void changeSize(int w, int h) {
	if(h == 0) h = 1;
	float ratio = 1.0* w / h;

	glMatrixMode(GL_PROJECTION);

	glLoadIdentity();

	glViewport(0, 0, w, h);

	gluPerspective(90,ratio,1,1000);

	glMatrixMode(GL_MODELVIEW);
}
void timer(int value){
    glutPostRedisplay();
    glutTimerFunc(refreshmil,timer,0);
}
int main(int argc, char **argv){
    string nama[8] = {"kopi.obj","meja_kopi.obj","meja_tv.obj","sofa.obj","terebi.obj","kipas_2.obj","sofa_2.obj","lemari_2.obj"};
    for(int i = 0;i<8;i++) loadObj(nama[i], i);//load banyak objek

    glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(1000, 600);

    //Glut create window section
	glutCreateWindow("Grafika Komputer | Proyek");

    //Glut proc set
    setupTexture();
	glutDisplayFunc(render);
	glutIdleFunc(render);


	glutReshapeFunc(changeSize);
	glutKeyboardFunc(keyDown);
	glutMouseFunc(mouseButton);
    glutMotionFunc(mouseMove);


    //glShadeModel (GL_SMOOTH);
    //---------------------------------
    glutTimerFunc(0,timer,0);

    //--------------------------------
    glutMainLoop();


    return 0;
}
