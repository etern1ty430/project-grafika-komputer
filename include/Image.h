#ifndef IMAGE_H
#define IMAGE_H


class Image
{
    public:
        Image(char* ps, int w, int h);
        ~Image();
        char* pixels;
        int width;
        int height;
};
#endif
Image* loadBMP(const char* filename);
